package com.wp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AddToCart {

	WebDriver webDriver;
	XSSFSheet sheet;
	@BeforeSuite
	public void getSheet() throws IOException {
		FileInputStream fis = new FileInputStream(new File("/Users/apple/eclipse-workspace/TestingTask/credentials/data.xlsx"));
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		this.sheet = workbook.getSheet("products");
		System.out.println("before suite ");
		System.out.println(this.sheet!=null);
//		workbook.close();
//		fis.close();
	}
	@BeforeTest
	public void browser() throws IOException {
		webDriver = Config.getBrowser();
		System.out.println("testing success");

	}

	@Test(priority = 1)
	public void cartManagement() throws InterruptedException, IOException {
		
//		//filling login form
//		webDriver.findElement(By.xpath("//input[@type=\"text\" and @class=\"_2zrpKA _1dBPDZ\"]")).sendKeys("9993331922");
//		webDriver.findElement(By.xpath("//input[@type=\"password\"]")).sendKeys("Ishu1997@25");
//		webDriver.findElement(By.xpath("//button[@type=\"submit\" and @class=\"_2AkmmA _1LctnI _7UHT_c\"]")).click();
		System.out.println("in test");
		LoginElements loginElements = new LoginElements(webDriver);
		loginElements.setMobile("9993331922");
		loginElements.setPassword("Ishu1997@25");
		loginElements.clickSubmit();
		Thread.sleep(5000);
		//if search bar is not active within 2 seconds then timeout exception
		//WebElement clickElement = webDriver.findElement(By.name("q"));
		WebElement searchElement = webDriver.findElement(By.xpath("//input[@placeholder=\"Search for products, brands and more\"]"));
		
		WebDriverWait wait = new WebDriverWait(webDriver, 2);
		wait.until(ExpectedConditions.elementToBeClickable(searchElement));

		Thread.sleep(5000);
		//searching book via search bar
		
		Iterator<Row> it = sheet.iterator();
		while(it.hasNext()) {
			searchElement.clear();
			Row row = it.next();
			String search = row.getCell(0).getStringCellValue();
			searchElement.sendKeys(search);
				
			webDriver.findElement(By.xpath("(//button)[1]")).sendKeys(Keys.ENTER);
			Thread.sleep(5000);

			//selecting product
			Actions action = new Actions(webDriver);
			WebElement element = webDriver.findElement(By.xpath("(//a)[590]"));
			Action build = action.moveToElement(element).click().build();
			build.perform();
			Thread.sleep(5000);
			//switching to new window and add to cart
			ArrayList<String> handle = new ArrayList<String>(webDriver.getWindowHandles());
			webDriver.switchTo().window(handle.get(1));
			
			Thread.sleep(2000);
			//taking screenshots of product added
			screenshot(webDriver,search);			
			Thread.sleep(2000);
			webDriver.findElement(By.xpath("(//button)[2]")).click();

//			//click place order 
//			webDriver.findElement(By.xpath("(//button)[4]")).click();
			Thread.sleep(3000);
			//closing current window switching to previous window
			webDriver.close();
			webDriver.switchTo().window(handle.get(0));
		}	
	}

	private void screenshot(WebDriver webDriver,String search) throws IOException {
		String path = System.getProperty("user.dir");
		TakesScreenshot ts = (TakesScreenshot)webDriver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(source,new File(path+"/screenshots/"+search+".png"));
		System.out.println("screenshot taken successfully of ===========>"+search);
		
	}
	@AfterTest
	public void quitBrowser() throws InterruptedException {
		Thread.sleep(5000);
		webDriver.quit();
	}
	
}
