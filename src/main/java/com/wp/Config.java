package com.wp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Config {
	
	static WebDriver webDriver;
	public static WebDriver getBrowser() throws IOException {
		
		String path = System.getProperty("user.dir");
		FileInputStream fis = new FileInputStream(new File("/Users/apple/eclipse-workspace/TestingTask/config.properties"));
		Properties prop = new Properties();
		prop.load(fis);
		
		String browserName = prop.getProperty("browser");
		String url = prop.getProperty("Environment");
		if(browserName.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", path+"/drivers/geckodriver");
			webDriver = new FirefoxDriver();
		}
		else {
			System.setProperty("webdriver.chrome.driver", path+"/drivers/chromedriver");
			webDriver = new ChromeDriver();
		}
		webDriver.manage().window().maximize();
		webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		webDriver.manage().timeouts().pageLoadTimeout(60,TimeUnit.SECONDS);
		webDriver.manage().deleteAllCookies();
		webDriver.get(url);
		return webDriver;
	}
}
