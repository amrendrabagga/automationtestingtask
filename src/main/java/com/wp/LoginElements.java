package com.wp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginElements {

	private WebDriver driver;
	
	By mobileField = By.xpath("//input[@type=\"text\" and @class=\"_2zrpKA _1dBPDZ\"]");
	By passField = By.xpath("//input[@type=\"password\"]");
	By submitField = By.xpath("//button[@type=\"submit\" and @class=\"_2AkmmA _1LctnI _7UHT_c\"]");
	
	public LoginElements(WebDriver driver) {
		this.driver = driver;
	}
	
	public void setMobile(String mobileNo) {
		driver.findElement(mobileField).sendKeys(mobileNo);
	}
	
	public void setPassword(String password) {
		driver.findElement(passField).sendKeys(password);
	}
	public void clickSubmit() {
		driver.findElement(submitField).click();
	}
}
